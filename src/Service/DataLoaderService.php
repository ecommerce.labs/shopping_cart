<?php

namespace App\Service;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class DataLoaderService
{
    /**
     * @var string
     */
    private $rootPath;

    /**
     * @var Filesystem
     */
    private $fileSystem;

    /**
     * @var Finder
     */
    private $finder;

    /**
     * File types
     */
    CONST JSON_FILE_TYPE = '.json';
    CONST YML_FILE_TYPE = '.yml';


    /**
     * DataLoaderService constructor.
     * @param $rootPath
     * @param Filesystem $fileSystem
     */
    public function __construct($rootPath, Filesystem $fileSystem)
    {
        $this->fileSystem = $fileSystem;
        $this->finder = new Finder();
        $this->rootPath = $rootPath;
    }


    /**
     * @param $className
     * @param string $fileType
     * @return string|null
     */
    public function getFileContentByClassName($className, $fileType = self::JSON_FILE_TYPE): ?string
    {
        if ($this->fileSystem->exists($this->rootPath)) {
            $this->finder->files()->in($this->rootPath);
            foreach ($this->finder as $file) {
                if (strstr($className, $file->getBasename($fileType))) {
                    return file_get_contents($file->getPathname());
                }
            }
        }

        return null;
    }
}