<?php
/**
 * Created by PhpStorm.
 * User: net
 * Date: 3/17/2019
 * Time: 6:11 PM
 */

namespace App\Service;


use App\Entity\Categories;
use App\Entity\OrderDetails;
use App\Entity\Orders;
use App\Entity\Products;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class ShoppingCartService
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    private $productsRepository;
    private $categoriesRepository;
    private $ordersRepository;
    private $orderDetailsRepository;
    private $session;

    public function __construct(
        EntityManagerInterface $entityManager,
        SessionInterface $session
    )
    {
        $this->entityManager = $entityManager;
        $this->categoriesRepository = $this->entityManager->getRepository(Categories::class);
        $this->productsRepository = $this->entityManager->getRepository(Products::class);
        $this->ordersRepository = $this->entityManager->getRepository(Orders::class);
        $this->orderDetailsRepository = $this->entityManager->getRepository(OrderDetails::class);
        //start session
        $this->session = $session;
        $this->session->start();
    }


    /**
     * @return array
     */
    public function getShoppingCartCategories(): array
    {
        return $this->categoriesRepository->getAllGroupedByNumberOfProducts();
    }


    /**
     * @param null $idCategory
     * @return array
     */
    public function getProducts($idCategory = null): array
    {
        if ($idCategory === null) {
            return $this->productsRepository->findAll();
        }

        return $this->productsRepository->findBy(['idCategory' => $idCategory]);

    }

    /**
     * @param $idProduct
     */
    public function addProductToCart($idProduct): void
    {
        if (!$this->session->has('cartItems')) {
            $this->session->set('cartItems', []);
        }

        $cartItems = $this->session->get('cartItems');
        if (!isset($cartItems[$idProduct])) {
            $cartItems[$idProduct] = [
                'item' => $this->productsRepository->findOneBy(['id' => $idProduct]),
                'qty' => 1
            ];
        } else {
            $cartItems[$idProduct]['qty']++;
        }

        $this->session->set('cartItems', $cartItems);
    }


    /**
     * @return array|null
     */
    public function getCartContent(): ?array
    {
        if ($this->session->has('cartItems')) {
            return $this->session->get('cartItems');
        }

        return null;
    }


    /**
     * @return null
     */
    public function eraseCartContent(): void
    {
        if ($this->session->has('cartItems')) {
            $this->session->clear();
        }
    }


    /**
     * @throws \Exception
     */
    public function createOrder(): void
    {
        if ($this->session->has('cartItems')) {
            $cartItems = $this->session->get('cartItems');
            $order = new Orders();
            $order->setAddedAt(new \DateTime());
            $this->entityManager->persist($order);
            $this->entityManager->flush();

            foreach ($cartItems as $item) {
                $orderDetails = new OrderDetails();
                $product = $this->productsRepository->findOneBy(['id' => $item['item']->getId()]);
                $orderDetails->setProduct($product);
                $orderDetails->setQuantity($item['qty']);
                $orderDetails->setOrder($order);

                $this->entityManager->persist($orderDetails);
            }

            $this->entityManager->flush();
        }

        $this->eraseCartContent();
    }


    public function getPlacedOrder(): array
    {
        return $this->ordersRepository->findAll();
    }
}