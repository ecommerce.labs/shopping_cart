<?php

namespace App\Service;

use App\Entity\ShoppingCartObjectInterface;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

class JsonToObjectService
{
    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * JsonToObjectService constructor.
     */
    public function __construct()
    {
        $this->serializer = new Serializer(
            [new GetSetMethodNormalizer(), new ArrayDenormalizer()],
            [new JsonEncoder()]
        );
    }

    /**
     * @param $jsonString
     * @param $className
     * @return ShoppingCartObjectInterface[]
     */
    public function transform($jsonString, $className): iterable
    {
        return $this->serializer->deserialize($jsonString, $className . '[]', 'json');
    }
}