<?php

namespace App\Command;

use App\Entity\Categories;
use App\Entity\Products;
use App\Service\DataLoaderService;
use App\Service\JsonToObjectService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Question\ChoiceQuestion;


class LoadProductsCommand extends Command
{
    private $dataLoaderService;
    private $entityManager;
    private $jsonToObjectService;

    protected static $defaultName = 'app:load-data:products';

    public function __construct(
        EntityManagerInterface $entityManager,
        DataLoaderService $dataLoaderService,
        JsonToObjectService $jsonToObjectService
    )
    {
        parent::__construct();

        $this->dataLoaderService = $dataLoaderService;
        $this->entityManager = $entityManager;
        $this->jsonToObjectService = $jsonToObjectService;
    }

    protected function configure(): void
    {
        $this
            ->setDescription("Loading products")
            ->setHelp("This command is used to load products in Database");
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        //Check if we have categories in db
        $categoryRepository = $this->entityManager->getRepository(Categories::class);
        $consoleCategoryList = $categoryRepository->getListForConsoleQuestion();

        if (count($consoleCategoryList) !== 0) {
            $jsonData = $this->dataLoaderService->getFileContentByClassName(Products::class);

            if ($jsonData !== null) {
                $objectData = $this->jsonToObjectService->transform($jsonData, Products::class);

                foreach ($objectData as $row) {
                    if ($row instanceof Products) {
                        //ask category for each
                        $helper = $this->getHelper('question');
                        $question = new ChoiceQuestion(
                            'Select category for product: ' . $row->getName(),
                            $consoleCategoryList,
                            0
                        );

                        $selectedCategory = $categoryRepository->findOneBy([
                            "name" => $helper->ask($input, $output, $question)
                        ]);

                        if ($selectedCategory instanceof Categories) {
                            $row->setCategory($selectedCategory);
                            $this->entityManager->persist($row);
                        }
                    }
                }
                //insert records into DB
                $this->entityManager->flush();
            }
        }
    }
}