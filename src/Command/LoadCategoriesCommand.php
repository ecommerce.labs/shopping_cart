<?php

namespace App\Command;

use App\Entity\Categories;
use App\Service\DataLoaderService;
use App\Service\JsonToObjectService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LoadCategoriesCommand extends Command
{
    /**
     * @var DataLoaderService
     */
    private $dataLoaderService;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var JsonToObjectService
     */
    private $jsonToObjectService;

    /**
     * @var string
     */
    protected static $defaultName = 'app:load-data:categories';


    /**
     * LoadCategoriesCommand constructor.
     * @param EntityManagerInterface $entityManager
     * @param DataLoaderService $dataLoaderService
     * @param JsonToObjectService $jsonToObjectService
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        DataLoaderService $dataLoaderService,
        JsonToObjectService $jsonToObjectService
    )
    {
        parent::__construct();

        $this->dataLoaderService = $dataLoaderService;
        $this->entityManager = $entityManager;
        $this->jsonToObjectService = $jsonToObjectService;
    }


    protected function configure(): void
    {
        $this
            ->setDescription("Loading product categories")
            ->setHelp("This command is used to load product categories in Database");
    }


    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $jsonData = $this->dataLoaderService->getFileContentByClassName(Categories::class);
        if ($jsonData !== null) {
            $objectData = $this->jsonToObjectService->transform($jsonData, Categories::class);

            $categoryCount = 0;
            foreach ($objectData as $row) {
                if ($row instanceof Categories) {
                    $this->entityManager->persist($row);
                    $categoryCount++;
                }
            }

            $this->entityManager->flush();

            $output->writeln("Number of categories inserted: " . $categoryCount);
        }
    }
}