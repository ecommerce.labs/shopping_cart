<?php

namespace App\Controller;

use App\Service\ShoppingCartService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    /**
     * @var ShoppingCartService
     */
    private $shoppingCartService;

    public function __construct(ShoppingCartService $shoppingCartService)
    {
        $this->shoppingCartService = $shoppingCartService;
    }

    /**
     * @Route("/cart", name="cart")
     */
    public function index()
    {
        return $this->render('cart/index.html.twig', [
            'cartItems' => $this->shoppingCartService->getCartContent()
        ]);
    }


    /**
     * @Route("/cart/add/{id_product}", name="cart_add")
     */
    public function addToCard($id_product)
    {
        $this->shoppingCartService->addProductToCart($id_product);

        return $this->redirectToRoute('cart');
    }


    /**
     * @Route("/cart/clear", name="cart_clear")
     */
    public function clearCart()
    {
        $this->shoppingCartService->eraseCartContent();

        return $this->redirectToRoute('cart');
    }
}
