<?php

namespace App\Controller;

use App\Service\ShoppingCartService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class OrdersController extends AbstractController
{

    /**
     * @var ShoppingCartService
     */
    private $shoppingCartService;

    public function __construct(ShoppingCartService $shoppingCartService)
    {
        $this->shoppingCartService = $shoppingCartService;
    }

    /**
     * @Route("/orders", name="orders")
     */
    public function index()
    {
        return $this->render('orders/index.html.twig', [
            'placedOrders' => $this->shoppingCartService->getPlacedOrder()
        ]);
    }

    /**
     * @Route("/orders/add", name="orders_add")
     */
    public function orderAdd()
    {
        $this->shoppingCartService->createOrder();

        return $this->redirectToRoute('orders');
    }
}
