<?php

namespace App\Controller;

use App\Service\ShoppingCartService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


class ProductsController extends AbstractController
{
    /**
     * @var ShoppingCartService
     */
    private $shoppingCartService;

    public function __construct(ShoppingCartService $shoppingCartService)
    {
        $this->shoppingCartService = $shoppingCartService;
    }

    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        return $this->render('products/index.html.twig', [
            'products' => $this->shoppingCartService->getProducts()
        ]);
    }


    /**
     * @Route("/category/{id_category}", name="category")
     */
    public function category($id_category)
    {
        return $this->render('products/index.html.twig', [
            'products' => $this->shoppingCartService->getProducts($id_category)
        ]);
    }
}
