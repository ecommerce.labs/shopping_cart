<?php

namespace App\Repository;

use App\Entity\Categories;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Categories|null find($id, $lockMode = null, $lockVersion = null)
 * @method Categories|null findOneBy(array $criteria, array $orderBy = null)
 * @method Categories[]    findAll()
 * @method Categories[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoriesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Categories::class);
    }

    /**
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countRows(): int
    {
        return $this->createQueryBuilder('c')
            ->select('COUNT(c)')
            ->getQuery()
            ->getSingleScalarResult();
    }


    /**
     * @return array
     */
    public function getListForConsoleQuestion(): array
    {
        $categories = $this->findAll();
        $consoleList = [];

        if ($categories) {
            for ($i = 0; $i < count($categories); $i++) {
                $consoleList[] = $categories[$i]->getName();
            }
        }

        return $consoleList;
    }

    public function getAllGroupedByNumberOfProducts(): array
    {
        return $this->createQueryBuilder('c')
            ->select('c.id, c.name, count(c) nb_products')
            ->innerJoin('c.products', 'p', 'WITH', 'c.id = p.idCategory')
            ->groupBy('c.id')
            ->orderBy('nb_products', 'DESC')
            ->getQuery()
            ->getResult();
    }
}
